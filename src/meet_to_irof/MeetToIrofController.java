/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package meet_to_irof;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import meet_to_irof.shimeji.ShimejiImageDirectory;

/**
 *
 * @author bitter_fox
 */
public class MeetToIrofController
{
    private final MeetToIrofModel model;
    private final DirectoryChooser dc = new DirectoryChooser();
    private Optional<ImageLoader> currentLoader = Optional.empty();

    public MeetToIrofController(MeetToIrofModel model)
    {
        this.model = model;
    }

    public void chooseShimejiDirectory(ActionEvent ae, Window w)
    {
        Path initDir = model.shimejiDirectoryProperty().get().toAbsolutePath();

        while (!Files.isDirectory(initDir))
        {
            initDir = initDir.getParent();
        }

        dc.setInitialDirectory(initDir.toAbsolutePath().toFile());
        File f = dc.showDialog(w);
        if (f != null)
        {
            model.setShimejiDirectory(f.toPath());
        }
    }

    public void onEditedShimejiDirectory(String directory)
    {
        Path dir = Paths.get(directory);

        model.setShimejiDirectory(dir);
    }

    public void loadImage(String url)
    {
        currentLoader.ifPresent(ImageLoader::cancel);

        ImageLoader l = new ImageLoader(url, ShimejiImageDirectory.IMAGE_WIDTH, ShimejiImageDirectory.IMAGE_HEIGHT);

        l.setOnSucceeded(e -> model.setIrofImage(l.getValue()));

        currentLoader = Optional.of(l);

        l.start();
    }

    public void meetToIrof(EventHandler<WorkerStateEvent> e)
    {
        if (!model.imageProperty().get().isPresent())
        {
            return;
        }

        Service<Void> service = new Service<Void>()
        {
            @Override
            protected Task<Void> createTask()
            {
                return new Task<Void>()
                {
                    @Override
                    protected Void call()
                    {
                        Path targetDir = model.shimejiDirectoryProperty().get();
                        ShimejiImageDirectory d = new ShimejiImageDirectory(targetDir);

                        long time = System.currentTimeMillis();

                        Map<Path, Path> map = d.getImageFiles().stream()
                            .filter(Files::exists)
                            .collect(Collectors.toMap(p -> p, Path::getFileName));
                        map.replaceAll(
                            (p1, p2) ->
                            {
                                String fileName = p2.toString();
                                int index = fileName.lastIndexOf('.');
                                if (index > 0)
                                {
                                    fileName = fileName.substring(0, index)
                                    + "_" + time
                                    + fileName.substring(index);
                                }
                                else
                                {
                                    fileName = fileName + "_" + time;
                                }

                                return p1.getParent().resolve(fileName);
                            });

                        map.forEach(
                            (p1, p2) ->
                            {
                                try
                                {
                                    Files.copy(p1, p2, StandardCopyOption.REPLACE_EXISTING);
                                }
                                catch (IOException e)
                                {
                                    e.printStackTrace();
                                }
                            });

                        d.replaceAllImages(model.imageProperty().get().get());

                        return null;
                    }
                };
            }
        };

        service.setOnSucceeded(e);
        service.setOnCancelled(e);
        service.setOnFailed(e);

        service.start();
    }
}
