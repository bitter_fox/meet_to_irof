/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package meet_to_irof.control;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

/**
 *
 * @author bitter_fox
 */
public class SizedImageView extends StackPane
{
    private Canvas background;
    private ImageView view;

    public SizedImageView(Canvas background, int width, int height)
    {
        this.background = background;

        background.setWidth(width);
        background.setHeight(height);

        view = new ImageView();

        view.setFitWidth(width);
        view.setFitHeight(height);
        view.setPreserveRatio(false);

        this.getChildren().add(background);
        this.getChildren().add(view);
    }

    public SizedImageView(Color color, int width, int height)
    {
        this(new Canvas(width, height), width, height);

        GraphicsContext context = background.getGraphicsContext2D();
        context.setFill(color);
        context.fillRect(0, 0, background.getWidth(), background.getHeight());
    }

    public ImageView getImageView()
    {
        return view;
    }
}
