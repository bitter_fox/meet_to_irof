/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package meet_to_irof;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.image.Image;

/**
 *
 * @author bitter_fox
 */
public class ImageLoader extends Service<Optional<Image>>
{
    private String url;
    private Optional<Integer> width = Optional.empty();
    private Optional<Integer> height = Optional.empty();

    public ImageLoader(String url)
    {
        this.url = url;
    }

    public ImageLoader(String url, int width, int height)
    {
        this.url = url;

        this.width = Optional.of(width);
        this.height = Optional.of(height);
    }

    protected Task<Optional<Image>> createTask()
    {
        return new Task<Optional<Image>>()
        {
            protected Optional<Image> call()
            {
                try
                {
                    URL url = new URL(ImageLoader.this.url);
                    Image i;

                    if (width.isPresent() && height.isPresent())
                    {
                        i = new Image(url.openStream(), width.get(), height.get(), false, true);
                    }
                    else
                    {
                        i = new Image(url.openStream());
                    }

                    return Optional.of(i);
                }
                catch (IOException | IllegalArgumentException e)
                {
                    return Optional.empty();
                }
            }
        };
    }
}
