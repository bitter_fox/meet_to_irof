/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package meet_to_irof;

import javafx.stage.Stage;

/**
 *
 * @author bitter_fox
 */
public class MeetToIrofContext
{
    private Stage stage;

    public MeetToIrofContext(Stage stage)
    {
        this.stage = stage;
    }

    public Stage getPrimaryStage()
    {
        return stage;
    }

}
