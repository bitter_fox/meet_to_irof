/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package meet_to_irof;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;

/**
 *
 * @author bitter_fox
 */
public class MeetToIrofModel
{
    private ObjectProperty<Path> shimejiDirectoryProperty = new SimpleObjectProperty<>(Paths.get(""));
    private ObjectProperty<Optional<Image>> imageProperty = new SimpleObjectProperty<>();

    public void setShimejiDirectory(Path dir)
    {
        shimejiDirectoryProperty.set(dir);
    }

    public ObjectProperty<Path> shimejiDirectoryProperty()
    {
        return shimejiDirectoryProperty;
    }

    public void setIrofImage(Optional<Image> image)
    {
        imageProperty.set(image);
    }

    public ObjectProperty<Optional<Image>> imageProperty()
    {
        return imageProperty;
    }
}
