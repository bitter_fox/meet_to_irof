/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package meet_to_irof;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import meet_to_irof.control.SizedImageView;
import meet_to_irof.shimeji.ShimejiImageDirectory;

/**
 *
 * @author bitter_fox
 */
public class MeetToIrofPane extends AnchorPane
{
    private final List<Label> labels = new ArrayList<>();
    private final DoubleProperty labelMinWidth = new SimpleDoubleProperty();

    private final MeetToIrofContext context;
    private final MeetToIrofModel model;
    private final MeetToIrofController controller;

    private Label lblImage;
    private TextField tfImage;
    private Canvas image;
    private SizedImageView imageView;

    private Label lblShimeji;
    private TextField tfShimeji;
    private Button btnShimeji;

    private Button btnMeetToIrof;

    public MeetToIrofPane(MeetToIrofContext context, MeetToIrofModel model, MeetToIrofController controller)
    {
        this.context = context;
        this.model = model;
        this.controller = controller;

        Pane root = createControls();

        registerController();

        registerModelViewing();

        AnchorPane.setTopAnchor(root, 1.0);
        AnchorPane.setBottomAnchor(root, 1.0);
        AnchorPane.setLeftAnchor(root, 1.0);
        AnchorPane.setRightAnchor(root, 1.0);
        this.getChildren().add(root);
    }

    protected Pane createControls()
    {
        lblImage = new Label("irof Image URL");
        labels.add(lblImage);
        tfImage = new TextField();
        image = new Canvas(128, 128);
        imageView = new SizedImageView(Color.GRAY, ShimejiImageDirectory.IMAGE_WIDTH, ShimejiImageDirectory.IMAGE_HEIGHT);

        lblShimeji = new Label("Shimeji Dir");
        labels.add(lblImage);
        tfShimeji = new TextField();
        btnShimeji = new Button("...");

        btnMeetToIrof = new Button("Meet to irof!!");

        BorderPane root = new BorderPane();
        {
            VBox center = new VBox();
            {
                BorderPane imagePane = new BorderPane();
                {
                    VBox _lblImage = new VBox();
                    {
                        _lblImage.setAlignment(Pos.BASELINE_RIGHT);
                        _lblImage.getChildren().add(lblImage);
                        _lblImage.minWidthProperty().bind(labelMinWidth);
                    }
                    imagePane.setLeft(_lblImage);
                    imagePane.setCenter(tfImage);
                }
                center.getChildren().add(imagePane);

                BorderPane shimejiPane = new BorderPane();
                {
                    VBox _lblShimeji = new VBox();
                    {
                        _lblShimeji.setAlignment(Pos.BASELINE_RIGHT);
                        _lblShimeji.getChildren().add(lblShimeji);
                        _lblShimeji.minWidthProperty().bind(labelMinWidth);
                    }
                    shimejiPane.setLeft(_lblShimeji);
                    shimejiPane.setCenter(tfShimeji);
                    shimejiPane.setRight(btnShimeji);
                }
                center.getChildren().add(shimejiPane);
            }
            root.setCenter(center);

            HBox bottom = new HBox();
            {
                bottom.getChildren().add(imageView);
                bottom.getChildren().add(btnMeetToIrof);
            }
            root.setBottom(bottom);
        }

        return root;
    }

    protected void registerController()
    {
        btnShimeji.setOnAction(e -> controller.chooseShimejiDirectory(e, context.getPrimaryStage()));
        tfShimeji.textProperty().addListener(
            (ov, o, n) ->
            {
                if (!o.equals(n))
                {
                    controller.onEditedShimejiDirectory(n);
                }
            });

        tfImage.textProperty().addListener(
            (ov, o, n) ->
            {
                controller.loadImage(n);
            });

        btnMeetToIrof.setOnAction(
            e ->
            {
                btnMeetToIrof.setDisable(true);
                controller.meetToIrof(ev -> btnMeetToIrof.setDisable(false));
            });
    }

    protected void registerModelViewing()
    {
        model.shimejiDirectoryProperty().addListener(
            (ov, o, n) ->
                tfShimeji.setText(n.toString()));
        model.imageProperty().addListener(
            (ov, o, n) ->
            {
                if (n.isPresent())
                {
                    imageView.getImageView().setImage(n.get());
                }
                else
                {
                    imageView.getImageView().setImage(null);
                }
            });
    }

    protected void layoutChildren()
    {
        super.layoutChildren();

        labelMinWidth.set(labels.stream()
            .mapToDouble(Label::getWidth)
            .reduce(Double::max).getAsDouble());
    }
}
