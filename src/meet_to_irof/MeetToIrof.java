/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package meet_to_irof;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author bitter_fox
 */
public class MeetToIrof extends Application
{
    @Override
    public void start(Stage primaryStage)
    {
        MeetToIrofContext context = new MeetToIrofContext(primaryStage);
        MeetToIrofModel model = new MeetToIrofModel();
        MeetToIrofController controller = new MeetToIrofController(model);
        MeetToIrofPane root = new MeetToIrofPane(context, model, controller);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Meet to irof");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }
}
