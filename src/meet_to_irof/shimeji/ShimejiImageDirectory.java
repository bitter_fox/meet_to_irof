/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package meet_to_irof.shimeji;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;

/**
 *
 * @author bitter_fox
 */
public class ShimejiImageDirectory
{
    private static final int IMAGE_FILE_MAX = 46;
    public static final int IMAGE_WIDTH = 128;
    public static final int IMAGE_HEIGHT = 128;

    private static final List<Path> IMAGE_FILES =
        IntStream.rangeClosed(1, IMAGE_FILE_MAX)
        .mapToObj(i -> "shime" + i + ".png")
        .map(Paths::get)
        .collect(Collectors.toList());

    private Path dir;
    private List<Path> imageFiles;

    public ShimejiImageDirectory(Path dir)
    {
        this.dir = dir;

        imageFiles = IMAGE_FILES.stream()
            .map(p -> dir.resolve(p))
            .collect(Collectors.toList());
    }

    public boolean isImageDirectory()
    {
        return imageFiles.stream()
            .allMatch(Files::exists);
    }

    public void replaceAllImages(Image image)
    {
        BufferedImage bi = SwingFXUtils.fromFXImage(image, null);
        this.getImageFiles().stream()
            .forEach(
                p ->
                {
                    try
                    {
                        ImageIO.write(bi, "png", p.toFile());
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                });
    }

    public List<Path> getImageFiles()
    {
        return imageFiles.stream().collect(Collectors.toList());
    }
}
